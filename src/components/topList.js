import React from "react";
import "../css/style.css";

class TopList extends React.Component {
  render() {
    return (
      <div>
        <section id="topList" className="brand-logos">
          <h1 className="sec-heading">
            Recognition as Top Mobile Development Company
          </h1>
          <div>
            {this.props.images.map((data) => {
              return (
                <a key={data.src} href="/">
                  <img
                    src={data.src}
                    alt="Top 10 MobleApp Development Companies"
                    title="Top 10 MobleApp Development Companies"
                  />
                  <span>
                    Recognised Among Top 10 MobleApp Development Companies
                  </span>
                </a>
              );
            })}
          </div>
        </section>
      </div>
    );
  }
}

//Exporting TopList globally as a component
export default TopList;
