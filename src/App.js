import { Component } from "react";
import Header from "./components/topHeader";
import Intro from "./components/intro";
import Delivery from "./components/delivery";
import Services from "./components/services";
import Success from "./components/success_story";
import Logos from "./components/brand_logos";
import Highlights from "./components/highlights";
import Partner from "./components/partners";
import TopList from "./components/topList";
import Footer from "./components/footer";
import "./css/style.css";

/**
 *brand_logos is the data object for navigation links. This data is passed down to <Header/> component.
 */
let brand_logos = [
  {
    src: "https://www.dropbox.com/s/lmvtthec9yn0ti6/Allianz.png?raw=1",
    alt: "Allianz",
    title: "Work with Allianz",
  },
  {
    src: "https://www.dropbox.com/s/kotgq2u4qr34i2u/audi.jpg?raw=1",
    alt: "Audi",
    title: "Work with Audi",
  },
  {
    src: "https://www.dropbox.com/s/t5dapt3lkz7rdhe/BMW.png?raw=1",
    alt: "BMW",
    title: "Work with BMW",
  },
  {
    src: "https://www.dropbox.com/s/ocqbsbgj590ztyy/ESPN.png?raw=1",
    alt: "ESPN",
    title: "Work with ESPN",
  },
  {
    src: "https://www.dropbox.com/s/2maaqxijcmbaqxg/LG.png?raw=1",
    alt: "LG",
    title: "Work with LG",
  },
  {
    src: "https://www.dropbox.com/s/yn3gj203hrdjfu7/Logo_NIKE.png?raw=1",
    alt: "Nike",
    title: "Work with Nike",
  },
  {
    src: "https://www.dropbox.com/s/gfxa6exv7h1ro6q/Suzuki_logo.png?raw=1",
    alt: "Suzuki",
    title: "Work with Suzuki",
  },
  {
    src: "https://www.dropbox.com/s/b7vwmjf6e0owybv/Visa.svg?raw=1",
    alt: "Visa",
    title: "Work with Visa",
  },
];

/**
 * logos2 is a data object for partner logos. These are passed down to <Partners/> component.
 */
let logos2 = [
  {
    src: "https://www.dropbox.com/s/mk5ca04seizpf8l/aws.svg?raw=1",
    alt: "Work with AWS",
    title: "Our Work",
  },
  {
    src: "https://www.dropbox.com/s/r9utt5nj9k9m1t8/Dell.png?raw=1",
    alt: "Dell",
    title: "Work with Dell",
  },
  {
    src: "https://www.dropbox.com/s/umw9g0zgm1ecfvn/Intel.png?raw=1",
    alt: "intel",
    title: "Work with intell",
  },
  {
    src: "https://www.dropbox.com/s/x0hrha2dosey99z/ibm.png?raw=1",
    alt: "IBM",
    title: "Work with IBM",
  },
  {
    src: "https://www.dropbox.com/s/ekzu1wcki6jziay/Microsoft.svg?raw=1",
    alt: "Microsoft",
    title: "WWork with Microsoft",
  },
  {
    src: "https://www.dropbox.com/s/lvl5cp14i3v0wgi/Nasscom.png?raw=1",
    alt: "Nasscom",
    title: "Work with Nasscom",
  },
  {
    src: "https://www.dropbox.com/s/h66k9jaaknxaum4/Samsung.png?raw=1",
    alt: "Samsung",
    title: "Work with Samsung",
  },
  {
    src: "https://www.dropbox.com/s/86cbtf78khj0q9z/Nvidia.png?raw=1",
    alt: "Nvidia",
    title: "Work with Nvidia",
  },
];

/**
 * icons is the data object for fav icons in services. This data is passed down to <Logos/> component.
 * It has two properties called class and title.
 */
let icons = [
  { class: "fas fa-laptop", title: "Stratagy and Consultant" },
  { class: "fas fa-users", title: "User Experience Design" },
  { class: "fas fa-mobile-alt", title: "Mobile App Development" },
  { class: "fab fa-chrome", title: "Web App Development" },
  { class: "fas fa-ribbon", title: "Quality Analysis and Testing" },
  { class: "fas fa-ticket-alt", title: "Application Management & Support" },
];

/**
 * navlinks is the data object for navigation links. This data is passed down to <Header/> component.
 */
let navlinks = [
  { title: "Technology" },
  { title: "Service" },
  { title: "Portfolio" },
  { title: "About Us" },
  { title: "Career" },
  { title: "Blog" },
];

/**
 * images is a data object for image source. This data is passed down to TopList component
 */
let images = [
  { src: "https://www.dropbox.com/s/19czj59oq0orbfa/tm.png?raw=1" },
  { src: "https://www.dropbox.com/s/130734rofy1f261/tata.png?raw=1" },
  {
    src: "https://www.dropbox.com/s/k17kwv9hiu9w98d/Infosys_logo.png?raw=1",
  },
  { src: "https://www.dropbox.com/s/mm4cnforc4pvwac/Wipro_Logo.png?raw=1" },
  {
    src: "https://www.dropbox.com/s/n4scpig8b3tfqkq/Amazon_logo.svg?raw=1",
  },
];

/**
 * This is passed down to <Footer/> component.
 */
let data = {
  Solution: ["Healthcare", "Sports", "ECommerce", "Construction", "Club"],
  Industry: [
    "Interprise App Development",
    "Android App Development",
    "ios App Development",
  ],
  "Quick Links": ["Reviews", "Terms & Condition", "Disclaimer", "Site Map"],
};

/**
 * This is a app class component having all the sectional components
 */
class App extends Component {
  render() {
    return (
      <>
        <Header navlinks={navlinks} />
        <Intro />
        <Delivery />
        <Services icons={icons} />
        <Success />
        <Logos brand_logos={brand_logos} />
        <Highlights />
        <Partner logos2={logos2} />
        <TopList images={images} />
        <Footer data={data} />
      </>
    );
  }
}

export default App;
