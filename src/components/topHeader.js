import React from "react";
import "../css/style.css";

class TopHeader extends React.Component {
  render() {
    return (
      <div>
        <header id="topHeader">
          <ul id="topInfo">
            <li>+974 98765432</li>
            <li>info@itecnology.com</li>
          </ul>

          <nav>
            <span className="logo">iTechnology</span>
            <div className="menu-btn-3" onClick={() => {}}>
              <span></span>
            </div>
            <div className="mainMenu">
              {this.props.navlinks.map((data) => {
                return (
                  <a href="/" key={data.title}>
                    <span>{data.title}</span>
                  </a>
                );
              })}
              <a href="/">Work With Us</a>
            </div>
          </nav>
        </header>
      </div>
    );
  }
}

export default TopHeader;
