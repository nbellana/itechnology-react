import React from "react";
import "../css/style.css";

class SuccessStories extends React.Component {
  render() {
    return (
      <div>
        <section id="success-story">
          <h1 className="sec-heading">Our Success Stories</h1>

          <div className="slider">
            <div className="col-6 slide-text">
              <div>
                <h2>World Travel Protection</h2>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text.
                </p>
                <a href="/" className="brand-btn">
                  Contact Us
                </a>
              </div>
            </div>
            <div className="col-6 slide-img">
              <img
                src="https://www.dropbox.com/s/ipx91osglyczpdt/delivery_experience.svg?raw=1"
                alt="World Travel App Development"
                title="World Travel Protection"
              />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default SuccessStories;
