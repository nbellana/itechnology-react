import React from "react";
import "../css/style.css";

class Highlights extends React.Component {
  render() {
    return (
      <div>
        <section id="highlights">
          <h1 className="sec-heading">Company Highlights</h1>

          <div className="slider">
            <div className="col-6 slide-text">
              <div>
                <h2>Team iTechnology at IBM, Americas 2019, Los Angeles</h2>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text.
                </p>
                <a href="/" className="brand-btn">
                  See More
                </a>
              </div>
            </div>
            <div className="col-6 slide-img">
              <img
                src="https://www.dropbox.com/s/vnkswx20c0dg5ta/analyzing.jpg?raw=1"
                alt="Team Work in Los Angeles"
                title="Company Team Work"
              />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Highlights;
