import React from "react";
import "../css/style.css";

class BrandLogos extends React.Component {
  render() {
    return (
      <div>
        <section id="revenue" className="brand-logos">
          <h1 className="sec-heading">
            We Drive Growth & Revenue for the Best Companies
          </h1>
          <div>
            {this.props.brand_logos.map((data) => {
              return (
                <a key={data.title} href="/">
                  <img src={data.src} alt={data.alt} title={data.title} />
                </a>
              );
            })}
          </div>
        </section>
      </div>
    );
  }
}

export default BrandLogos;
