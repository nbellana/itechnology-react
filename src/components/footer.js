import React from "react";
import "../css/style.css";

class Footer extends React.Component {
  SocialLinks = (props) => {
    return (
      <>
        <div className={props.class}>
          <a href="/">
            <i className="fab fa-facebook-f"></i>
          </a>
          <a href="/">
            <i className="fab fa-twitter"></i>
          </a>
          <a href="/">
            <i className="fab fa-linkedin-in"></i>
          </a>
          <a href="/">
            <i className="fab fa-instagram"></i>
          </a>
          <a href="/">
            <i className="fab fa-tumblr"></i>
          </a>
          <a href="/">
            <i className="fab fa-reddit-alien"></i>
          </a>
        </div>
      </>
    );
  };

  render() {
    return (
      <>
        <footer>
          <div>
            <span className="logo">iTechnology</span>
          </div>

          <div className="row">
            {Object.entries(this.props.data).map(([key, value]) => {
              return (
                <div className="col-3" key={key}>
                  <span className="footer-cat">{key}</span>
                  <ul className="footer-cat-links">
                    {value.map((col) => {
                      return (
                        <li key={col}>
                          <a href="/">
                            <span>{col}</span>
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              );
            })}

            <div className="col-3" id="newsletter">
              <span className="footer-cat">Stay Connected</span>
              <form id="subscribe">
                <input
                  type="email"
                  id="subscriber-email"
                  placeholder="Enter Email Address"
                />
                <input type="submit" value="Subscribe" id="btn-scribe" />
              </form>

              <this.SocialLinks className="social-links social-2" />

              <div id="address">
                <span className="footer-cat">Office Location</span>
                <ul>
                  <li>
                    <i className="far fa-building"></i>
                    <div>
                      Los Angeles
                      <br />
                      Office 9B, Sky High Tower, New A Ring Road, Los Angeles
                    </div>
                  </li>
                  <li>
                    <i className="fas fa-gopuram"></i>
                    <div>
                      Delhi
                      <br />
                      Office 150B, Behind Sana Gate Char Bhuja Tower, Station
                      Road, Delhi
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <this.SocialLinks className="social-links social-1 col-6" />
          </div>

          <div id="copyright">&copy; All Rights Reserved 2019-2020</div>
          <div id="owner">
            <span>
              Designed by
              <a href="https://www.codingtuting.com">CodingTuting.Com</a>
            </span>
          </div>
          <a href="#topHeader" id="gotop">
            Top
          </a>
        </footer>
      </>
    );
  }
}
export default Footer;
