import React from "react";
import "../css/style.css";

class Partners extends React.Component {
  render() {
    return (
      <div>
        <section id="partners" className="brand-logos">
          <h1 className="sec-heading">Our Partners</h1>
          <div>
            {this.props.logos2.map((data) => {
              return (
                <a key={data.title} href="/">
                  <img src={data.src} alt={data.alt} title={data.title} />
                </a>
              );
            })}
          </div>
        </section>
      </div>
    );
  }
}

export default Partners;
