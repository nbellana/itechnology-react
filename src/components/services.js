import React from "react";
import "../css/style.css";

class Services extends React.Component {
  render() {
    return (
      <div>
        <section id="services">
          <h1 className="sec-heading">Our Services</h1>
          <ul>
            {this.props.icons.map((data) => {
              return (
                <li key={data.title}>
                  <div>
                    <a href="/">
                      <i className={data.class}></i>
                      <span>{data.title}</span>
                    </a>
                  </div>
                </li>
              );
            })}
          </ul>

          <div id="service-footer">
            <a href="/" className="brand-btn">
              View All Service
            </a>
          </div>
        </section>
      </div>
    );
  }
}

export default Services;
